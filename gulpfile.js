var gulp = require('gulp'),
    less = require('gulp-less'),
    browserSync = require('browser-sync'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglifyjs'),
    del = require('del'),  // библеотека для удаления файлов и папок
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    cache = require('gulp-cache'),
    autoprefixer = require('gulp-autoprefixer');

gulp.task('default', function(){
    console.log('magic');
});

gulp.task('less', function(){
    return gulp.src('app/less/**/*.less')
               .pipe(less())  // преобразовани less в css
               .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade:true}))
               .pipe(gulp.dest('app/css'))  // выгружаем результаты
               .pipe(browserSync.reload({ stream: true}))  // меняем стили на странице если css меняется
})


gulp.task('browser-sync', function(){
    browserSync({
        server: {
            baseDir: 'app'
        },
        notify: false
    })
});

gulp.task('clean',function(){
    return del.sync('dist');  // delete folder dist 
});

gulp.task('img', function(){
    return gulp.src('app/img/**/*')
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{ removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('dist/img'));
})

gulp.task('build',['clean','img','less'], function(){
    var buildCSS = gulp.src([
        'app/css/*.css'
    ])
    .pipe(gulp.dest('dist/css'));

    var buildJS = gulp.src('app/js/**/*.js')
            .pipe(gulp.dest('dist/js'));

    var buildHtml = gulp.src('app/*.html')
        .pipe(gulp.dest('dist')); 
})
gulp.task('watch',['browser-sync', 'less'], function(){  // запустить browserSync перед watch
    gulp.watch('app/less/**/*.less', ['less']);
    gulp.watch('app/*.html', browserSync.reload);
    gulp.watch('app/js/**/*.js', browserSync.reload);
    gulp.watch('app/js/**/*.html', browserSync.reload)
    // наблюдение за другими типами файлов
})

gulp.task('clear', function(){
    return cache.clearAll();
})

gulp.task('default',['watch']);