app.config(function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'template/head.html',
        controller: 'newsCtrl'
    })
    $routeProvider.when('/news/:id', {
        templateUrl: 'template/detail.html',
        controller: 'detail'
    });
    $routeProvider.otherwise({ redirectTo: '/' })
})