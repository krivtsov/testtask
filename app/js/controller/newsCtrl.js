app.controller('newsCtrl', function ($scope, $routeParams) {
    $scope.category = [
        {
            name: 'all'
        }, {
            name: 'News'
        }, {
            name: 'Sport'
        }, {
            name: 'World'
        }
    ]


    $scope.news = [
        {
            title: 'У Києві під час пожежі в РДА евакуювали сотню чиновників',
            details: 'Займання почалося в електрощитовій на першому поверсі будівлі, зазначили рятувальники.',
            img: 'img/1.jpg',
            type: 'News',
            id: 1
        }, {
            title: 'Одеський припортовий завод хочуть продати за 5 млрд гривень',
            details: 'При цьому конкурс назвуть таким, що не відбувся, якщо інтересантів буде менше двох.',
            img: 'img/2.jpg',
            type: 'News',
            id: 2
        }, {
            title: 'Сирійський танк двічі уникнув американської ракети',
            details: 'Т-72 армії Башара Асада уникнув зустрічі з TOW.',
            img: 'img/3.jpg',
            type: 'World',
            id: 3
        }, {
            title: 'У зоні АТО загинув український матрос',
            details: 'Сепаратисти обстріляли позиції українських військових з підствольних гранатометів.',
            img: 'img/4.jpg',
            type: 'News',
            id: 4
        }, {
            title: 'Луценко призначив нового прокурора Запорізької області',
            details: 'Валерій Романов повинен відновити взаємодію з усіма гілками влади в регіоні.',
            img: 'img/5.jpg',
            type: 'News',
            id: 5
        }, {
            title: 'Вибух у Кіровоградській області, є постраждалі',
            details: "П'яний чоловік підірвав вибуховий пристрій.",
            img: 'img/6.jpg',
            type: 'News',
            id: 6
        }, {
            title: 'Британський фунт може впасти ще на 10% - Goldman Sachs',
            details: 'На думку аналітиків, британська нацвалюта "поки ще не дешева.',
            img: 'img/7.jpg',
            type: 'World',
            id: 7
        }, {
            title: ' МВФ прогнозує дешеву нафту до 2021 року',
            details: 'Ціни на нафту сьогодні коливаються в районі 52-52,5 долара.',
            img: 'img/8.jpg',
            type: 'World',
            id: 8
        }, {
            title: "Банки зобов'язали не використовувати платіжні системи РФ",
            details: "Вони також повинні завершити взаєморозрахунки за здійсненими операціями.",
            img: 'img/9.jpg',
            type: 'World',
            id: 9
        }, {
            title: 'На Волині під час вибуху на заводі загинув співробітник - ЗМІ',
            details: 'Ще двоє людей постраждали, одна з них в реанімації.',
            img: 'img/10.jpg',
            type: 'News',
            id: 10
        }, {
            title: 'Світоліна легко виходить до третього кола Кубка Кремля',
            details: 'Українка Еліна Світоліна без особливих проблем здолала казашку Юлію Путинцеву і пробилася до чвертьфіналу московських змагань.',
            img: 'img/11.jpg',
            type: 'Sport',
            id: 11
        }, {
            title: ' Бейонсе вийшла в світ у прозорій сукні',
            details: 'Знімки артистки в сміливому вбранні були опубліковані в її Instagram.',
            img: 'img/12.jpg',
            type: 'World',
            id: 12
        }, {
            title: 'Сирія визнала Крим частиною Росії',
            details: `Глава сирійського парламенту назвала півострів "невід'ємною частиною Росії".`,
            img: 'img/13.jpg',
            type: 'News',
            id: 13
        }, {
            title: ' Як минула остання вечеря Обами ',
            details: 'Президентський термін Барака Обами добігає кінця.',
            img: 'img/14.jpg',
            type: 'Sport',
            id: 14
        }, {
            title: 'У МВС скаржаться на недостатнє фінансування у 2017 році',
            details: 'У проекті держбюджету на силовиків виділено понад 41 млрд.',
            img: 'img/15.jpg',
            type: 'World',
            id: 15
        }, {
            title: 'ЗМІ порівняли камеру Google-смартфона і конкурентів',
            details: 'Крім смартфона Pixel XL в тесті також задіяли iPhone 7 і Galaxy S7 Edge.',
            img: 'img/16.jpg',
            type: 'World',
            id: 16
        }

    ];

    $scope.currentPage = 0;
    $scope.itemsPerPage = 6;
    $scope.startingItem = function () {
        return $scope.currentPage * $scope.itemsPerPage;
    }
    $scope.pageBack = function () {
        $scope.currentPage = $scope.currentPage - $scope.itemsPerPage;
    };
    $scope.pageForward = function () {
        $scope.currentPage = $scope.currentPage + $scope.itemsPerPage;
    }
    $scope.currentFrom = 'all';

    $scope.filter = function (name) {
        $scope.currentFrom = name;
    }


});