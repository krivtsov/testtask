app.controller('detail', function ($scope, $location, $route, $routeParams) {
    $scope.hello = $route.current.params.id;

    $scope.deta = {
        1: {
            title: 'У Києві під час пожежі в РДА евакуювали сотню чиновників',
            detail1: `
            У Києві загорілася чотириповерхова будівля Деснянської райадміністрації, повідомили в прес-службі столичної ДСНС.
            У Києві під час пожежі в РДА евакуювали сотню чиновників
                Пожежа сталася сьогодні, 19 жовтня, близько 11:50 на вулиці Маяковського.Как выяснилось, возгорание началось на первом этаже в электрощитовой.">Як з'ясувалося, займання почалося на першому поверсі в електрощитовій. Для тушения использовали пять порошковых          огнетушителя, на месте работали около 30 спасателей.
            Для гасіння використовували п'ять порошкових вогнегасники, на місці працювали близько 30 рятувальників.
            `,
            detail2: `
            Також під час пожежі евакуювали до сотні чиновників, зазначили в прес-службі.Сейчас устанавливают причину возгорания.
            Зараз встановлюють причину займання.
           
            Как сообщал Корреспондент.net, в Киеве горела школа академической гребли.
 
            Як повідомляв Кореспондент.net, у Києві горіла школа академічного веслування.
        В Киеве горел дом: эвакуировали почти 40 человек">У Києві горів будинок: евакуювали майже 40 осіб.
            `,
            img: 'img/1.jpg'
        },
        2: {
            title: 'Сирійський танк двічі уникнув американської ракети',
            detail1: `Кабінет міністрів схвалив умови повторного конкурсу з продажу ПАТ Одеський припортовий завод (ОПЗ),
         визначивши стартову ціну пакета акцій на рівні 5,16 млрд гривень (200 млн доларів), передає "По словам главы Фонда Игоря Билоуса, конкурс назовут несостоявшимся, 
         если интересантов будет меньше двух.">За словами голови Фонду Ігоря Білоуса, конкурс назвуть таким, що не відбувся, якщо інтересантів буде менше двох.`
            , detail2: `За словами голови Фонду Ігоря Білоуса, конкурс назвуть таким, що не відбувся, якщо інтересантів буде менше двох. 
         Сам Білоус попит на ОПЗ визначив як "середньо активний". Раніше глава Фонду держмайна Ігор Білоус заявив, що стартову ціну на Одеський припортовий завод (ОПЗ) потрібно знизити до 150 мільйонів доларів.`
            , img: 'img/2.jpg'
        },
        3: {
            title: 'У зоні АТО загинув український матрос',
            detail1: `Танк Т-72 сирійської армії двічі ухилився від американської протитанкової керованої ракети TOW. Про це свідчить відеозапис на YouTube.
        За твердженням авторів, знята робота танка Т-72 на позиції на північ від Хами. Машина двічі викочується із-за імпровізованого бруствера, ведучи вогонь по бойовиках.`,
            detail2: ``,
            img: 'img/3.jpg'
        },
        4: {
            title: 'Луценко призначив нового прокурора Запорізької області',
            detail1: `У зоні АТО під час виконання бойових завдань загинув український моряк, повідомили ВМС ЗСУ у Facebook.Ім'я загиблого: Дмитро Андрійович Захаров. 
        У той же час інформація про обставини загибелі матроса не уточнюється.
        На сторінці "Морська піхота України" у Facebook зазначається, що загиблому було 22 роки.
 `,
            detail2: `
                "У цей день він ніс службу на оглядовому посту. Ворожа ДРГ обстріляла позиції з підствольних гранатометів - і Дмитро був смертельно поранений осколками", - сказано в повідомленні.
                Спікер Міноборони з питань АТО Андрій Лисенко повідомив, що за добу в зоні проведення антитерористичної операції один військовий загинув і один отримав поранення.
                За його словами, це сталося в районі Лебединського в результаті обстрілу.
                Як повідомлялося, за минулу добу 57 разів відкривали вогонь по позиціях українських військових на Донбасі.
                За словами президента Петра Порошенка, на Донбасі загинули 2,5 тисячі військових.
        `,
            img: 'img/4.jpg'
        },
        5: {
            title: 'Вибух у Кіровоградській області, є постраждалі',
            detail1: `Сьогодні генпрокурор України Юрій Луценко представив правоохоронцям Запорізької області нового регіонального прокурора Валерія Романова, передає Інтерфакс-Україна.

                Луценко подякував екс-прокурору Запорізької області Олександру Шацькому, який займав цю посаду "в непростий час", наголосивши, що він патріот і один з тих, хто втримав українську владу в регіоні в 2014 році.

                Говорячи про причини звільнення Шацького, Луценко серед інших зазначив, що в останній рік відбулися неприпустимі публічні протистояння між обласним прокурором і головою Запорізької обласної адміністрації Костянтином Брилем.`,
            detail2: `
                Крім того, він зазначив, що Шацький в інтерв'ю одній з газет заявив, що голова Запорізької області сяде. "Не маєш фактів - не говори про це", - переконаний Юрій Луценко.

                За словами генпрокурора, новопризначений прокурор області Валерій Романов повинен відновити взаємодію з усіма гілками влади в регіоні, припинити піар-війну в ЗМІ, навпаки - разом з ними "воювати зі спільним ворогом".

                Як повідомляв Кореспондент.net, раніше стало відомо, що прокурора Запорізької області Олександра Шацького звільнять за підсумками перевірки Служби безпеки "за витік інформації".
        `,
            img: 'img/5.jpg'
        },
        6: {
            title: 'Британський фунт може впасти ще на 10% - Goldman Sachs',
            detail1: `У Бобринецькому районі Кіровоградської області нетверезий чоловік підірвав вибуховий пристрій, у результаті чого двоє людей отримали осколкові поранення ніг, 
        повідомили в місцевій поліції.
        45-річний п'яний чоловік ввечері, 17 жовтня, біля одного з розважальних закладів запросив незнайомих йому людей відпочити разом, після відмови вирішив "поквитатися за неповагу".`,
            detail2: `Чоловік дістав невідомий пристрій і жбурнув його в протилежний від себе бік. Предмет скотився по сходах у підвальне приміщення неподалік і там вибухнув.
 
        У  результаті двоє чоловіків (22 і 23 років) отримали осколкові поранення ніг. Правопорушника затримали, пояснити мотиви свого вчинку він не зміг.
 
        Відкрите кримінальне провадження, триває розслідування.
 
        Як повідомляв Корреспондент.net, на Волині під час вибуху на заводі загинув робітник.`,
            img: 'img/6.jpg'
        },
        7: {
            title: ' МВФ прогнозує дешеву нафту до 2021 року',
            detail1: `Курс фунта стерлінгів може впасти ще на 10%, прогнозують аналітики Goldman Sachs. На їхню думку, британська нацвалюта "поки ще не дешева", незважаючи на сильне падіння курсу щодо кошика основних світових валют з моменту голосування британців за вихід з Євросоюзу.

                "З кінця червня нинішнього року, коли жителі країни в ході референдуму підтримали ідею про вихід Британії зі складу ЄС, курс фунта до валютного кошика знизився майже на 15%", - пише The Times.

                Як зазначає видання, нещодавно експерти Goldman Sachs передбачали зниження курсу фунта до $1,20.Аналогічної точки зору дотримуються і аналітики HSBC. На їхню думку, наступна ключова віха на шляху падаючого фунта - $1,10.

                Тим часом в BNP Paribas впевнені у зворотному. За словами експертів французького банку, "фунт сьогодні значно недооцінений" і може найближчим часом відіграти позиції, піднявшись до $1,29.

                Ще один французький банк Societe Generale заявив, що готовий купувати фунти, як тільки курc досягне $1,15.

                На закритті ринку у вівторок курс фунта перебував на позначці $1,2298. В ході торгів в середу він піднявся до $1,2320 після публікації позитивних даних про зайнятість у 
                Великобританії в серпні, зокрема, більш значного підвищення зарплат в порівнянні з очікуваннями експертів.`,
            detail2: `Як повідомляв Кореспондент.net, 7 жовтня на валютний торгах в Токіо курс фунта стерлінгів до долара знову опустився нижче від мінімуму з 1985 року. Так, за один фунт в той день давали 1,184 долара. У порівнянні із закриттям біржі в Нью-Йорку британський фунт втратив 1,4%.

                Зазначимо, що курс британської валюти коливається через рішення Британії залишити Євросоюз. Зазначимо, що після референдуму щодо Brexit фунт впав на 14 відсотків до долара і завершив свій найгірший квартал з 1984 року.

                4 серпня Банк Англії опустив облікову ставку до мінімуму за 322 роки.`,
            img: 'img/7.jpg'

        },
        8: {
            title: "Банки зобов'язали не використовувати платіжні системи РФ",
            detail1: `
                Ціни на нафту сьогодні коливаються в районі 52-52,5 долара.
                Ціни на нафту не піднімуться вище від позначки в 60 доларів за барель аж до 2021 року. Про це йдеться в доповіді МВФ.

                У 2016 році середня ціна бареля складе 43 долари, в 2017 році підніметься до 51 долара, але подальше зростання буде обмежене.
                При цьому в звіті наголошується, що глобальні ризики, пов'язані з можливими перебоями поставок нафти, а також підвищенням ефективності видобутку сланцевої нафти в США, 
                можуть привести до значного підвищення нафтових котирувань, так і до їхнього падіння.
        `,
            detail2: `
                У ході сьогоднішніх торгів ціна нафти марки Brent коливається в районі 52-52,5 долара за барель.

                Раніше Саудівська Аравія заявила, що ціни на нафту досягнуть 60 доларів за барель до кінця 2016 року.

                Зазначимо, що аналітики Goldman Sachs прогнозують, що ціни на нафту зростуть на 10 доларів в наступному році завдяки угоді ОПЕК.
        `,
            img: 'img/8.jpg'

        },
        9: {
            title: "Банки зобов'язали не використовувати платіжні системи РФ",
            detail1: `Нацбанк зобов'язав банки і небанківські установи припинити надання послуг російських платіжних систем, повідомляється на сайті регулятора.

Банки також повинні завершити взаєморозрахунки за здійсненими операціями та повернути в НБУ свідоцтва про реєстрацію участі в таких міжнародних платіжних системах.`,
            detail2: `Про заборону російських платіжних систем в Україні повідомлялося вчора. Вводиться заборона на здійснення діяльності в Україні платіжних систем Золота Корона, Колібрі (раніше - Бліц), Міжнародні грошові перекази Лідер, Юністрім, Anelik та Blizko.

Підставою для заборони стало продовження санкцій України проти Росії 17 жовтня.`,
            img: 'img/9.jpg'

        },
        10: {
            title: 'На Волині під час вибуху на заводі загинув співробітник - ЗМІ',
            detail1: `Нацбанк зобов'язав банки і небанківські установи припинити надання послуг російських платіжних систем, повідомляється на сайті регулятора.

Банки також повинні завершити взаєморозрахунки за здійсненими операціями та повернути в НБУ свідоцтва про реєстрацію участі в таких міжнародних платіжних системах.`,
            detail2: `Про заборону російських платіжних систем в Україні повідомлялося вчора. Вводиться заборона на здійснення діяльності в Україні платіжних систем Золота Корона, Колібрі (раніше - Бліц), Міжнародні грошові перекази Лідер, Юністрім, Anelik та Blizko.

Підставою для заборони стало продовження санкцій України проти Росії 17 жовтня.`,
            img: 'img/10.jpg'

        },
        11: {
            title: 'Світоліна легко виходить до третього кола Кубка Кремля',
            detail1: `Сьогоднішньою суперницею четвертої ракетки Кубка Кремля була представниця Казахстану, яка востаннє вигравала в Еліни чотири роки тому. Тоді ще зовсім юні дівчата зустрічалися на турнірі в Дубаї, і Юлія святкувала перемогу в трьох сетах.`,
            detail2: `Зараз же ситуація істотно змінилася - Світоліна є 15-ю ракеткою світу, а Путинцева при непоганому тенісі все частіше демонструє погані результати. Лише на ґрунтових покриттях вона відчуває себе більш-менш впевнено, що підтверджує її вихід в чвертьфінал Ролан Гаррос-2016.

Наступною суперницею першої ракетки України стане переможниця пари Ганна Блінкова - Ана Коньюх.`,
            img: 'img/11.jpg'

        },
        12: {
            title: ' Бейонсе вийшла в світ у прозорій сукні',
            detail1: `Американська співачка Бейонсе вийшла в світ в абсолютно прозорій відвертій сукні, що підкреслює її форми. Знімки, на яких артистка відображена в сміливому вбранні, з'явилися на її сторінці в Instagram і встигли зібрати понад 1,5 мільйона лайків.`,
            detail2: `Чорна сукня до підлоги була прикрашена чорним камінням. Як зазначила сама артистка, вбрання є ручною роботою.Свій яскравий образ вона також доповнила масивними браслетами і великими сережками в тон вбранню.`,
            img: 'img/12.jpg'

        },
        13: {
            title: 'Сирія визнала Крим частиною Росії',
            detail1: `Голова парламенту Сирії Хадія Аббас в інтерв'ю Sputnik заявила, що Крим є невід'ємною частиною Росії.`,
            detail2: `"Ми визнаємо, що Крим - невід'ємна частина Росії. Відносини між САР та РФ є стратегічними і перспективними", - сказала Аббас.

У цьому ж інтерв'ю політик стверджує, що Росія "завжди приймала сторону пригноблених, тих, кому загрожувала агресія", а президент Путін і міністр закордонних справ Лавров "захищають Сирію так, як ніби захищають власну країну".
Як повідомляв Кореспондент.net, 18 травня рада Венеції визнала Крим російським.
`,
            img: 'img/13.jpg'

        },
        14: {
            title: ' Як минула остання вечеря Обами ',
            detail1: `Остання державна вечеря президента США Барака Обами і його дружини відбулася в наметі на галявині Білого дому, пише USA Today.

Подружжя розділило трапезу з прем'єр-міністром Італії Маттео Ренці і його дружиною Аньєзе Ландіні.

Мішель Обама вдягнула на свою останню вечерю в ролі першої леді США сукню від Versace, яку модний італійський будинок зшив спеціально для неї.`,
            detail2: `Вона виконана на манер грецької туніки в кольорі рожеве золото. Модель отримала назву Flotus.

Як розповів шеф-кухар Білого дому Маріо Баталі, на закуску гостям подавали равіолі з солодкою картоплею, потім - салат з мускатного гарбуза і мармурову яловичину на шпажках, а на десерт пропонували італійський яблучний тарт з морозивом.

З нагоди останньої державної вечері Барака Обами на сайті адміністрації президента США згадали найзначніші зустрічі в Білому домі.`,
            img: 'img/14.jpg'

        },
        15: {
            title: 'У МВС скаржаться на недостатнє фінансування у 2017 році',
            detail1: `У проекті держбюджету на силовиків виділено понад 41 млрд.
У МВС поскаржилися на недостатнє фінансування, передбачене в проекті бюджету на 2017 рік. За словами заступника міністра внутрішніх справ України Олексія Тахтая, витрати, передбачені проектом Держбюджету, набагато менші від бюджетного запиту.

"Ми зменшували розмір бюджетного запиту до критичної потреби. Якщо цю цифру не закласти для Міністерства внутрішніх справ, то "велосипед реформ", який почав їхати, може зупинитися і впасти", - заявив Тахтай під час обговорення з представниками громадськості проекту бюджету.`,
            detail2: `Також заступник міністра наголосив на важливості підвищення рівня забезпечення Держслужби України з надзвичайних ситуацій (ДСНС).

"Працівники ДСНС, нехай і не на фронті, щодня ризикують життям, рятуючи людей. Їхня зарплата не повинна бути низькою в системі", - сказав Тахтай, додавши, що зараз мінімальна зарплата в ДСНС становить 3 тисячі гривень.

Голова Громадської ради при МВС України Володимир Мартиненко поінформував, що в проекті бюджету замість майже 60 млрд грн на фінансування МВС виділено трохи більше 41 мільярда.

"При такому фінансуванні є тільки два варіанти розвитку подій: або скорочення особового складу, або різке зменшення зарплати", - заявив Мартиненко.

Раніше глава Нацполіції Хатія Деканоїдзе заявила, що на реформу МВС потрібно ще $200 мільйонів.`,
            img: 'img/15.jpg'

        },
        16: {
            title: 'ЗМІ порівняли камеру Google-смартфона і конкурентів',
            detail1: `Крім смартфона Pixel XL в тесті також задіяли iPhone 7 і Galaxy S7 Edge.
Автор видання Mashable Раймон Вон порівняв можливості камери нового смартфона Pixel XL з iPhone 7 і Galaxy S7 Edge. На думку журналіста, в знімках смартфона Google перенасичені кольори, проте його камеру можна поставити в один ряд з камерами конкурентів.`,
            detail2: `У ході тестування автором до уваги бралися автофокусування, передача кольору, а також динамічний діапазон і рівень деталізації.

Крім перенасиченості кольорів в знімках камери Pixel XL, Вон також помітив не завжди точний баланс білого. При цьому він зазначив, що фото Google-смартфона часто виграють за рахунок програмної обробки.

У цілому ж, на думку експерта, камера Pixel XL дозволяє робити якісні знімки з широким динамічним діапазоном в будь-яких умовах.`,
            img: 'img/16.jpg'

        }
    };
    for (var key in $scope.deta) {
        if (key = $route.current.params.id)
            $scope.active = $scope.deta[key];
    }

    $scope.share = {
        vk() {
            url = `http://vkontakte.ru/share.php?`;
            url += `url= URL`;
            url += '&title=' + $scope.active.title;
            url += '&noparse=true';
            console.log(url);
            $scope.share.popup(url);
        },
        facebook() {
            url = 'http://www.facebook.com/sharer.php?s=100';
            url += '&p[title]=' +  $scope.active.title;
            url += '&p[url]=' + 'URL';
            $scope.share.popup(url);
        },
        twitter(){
        url  = 'http://twitter.com/share?';
        url += 'text='      + $scope.active.title;
        url += '&url='      + encodeURIComponent('URL');
        url += '&counturl=' + encodeURIComponent('');
        $scope.share.popup(url);
        },
        me(el) {
            share.popup(el.href);
        },
        popup(url) {
            window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
        }
    }



})
